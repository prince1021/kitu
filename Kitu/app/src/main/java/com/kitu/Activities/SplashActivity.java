package com.kitu.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kitu.R;

public class SplashActivity extends AppCompatActivity {
    private long timeInterval = 3000;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            public void run() {
                // TODO Auto-generated method stub
                Intent chooseIntent = null;
                chooseIntent = new Intent(SplashActivity.this, LogoActivity.class);
                startActivity(chooseIntent);
                chooseIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                SplashActivity.this.overridePendingTransition(R.anim.slide_in_from_right,
                        R.anim.slide_out_to_left);
                finish();
            }

        }, timeInterval);
    }
}
