package com.kitu.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.graphics.BitmapCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kitu.Module.BitmapUtil;
import com.kitu.Module.ImageUtils;
import com.kitu.R;
import com.kitu.Item.tempItem;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;


/**
 * Created by PRINCE on 11/30/2016.
 */

public class RegisterServerActivity extends Activity implements View.OnClickListener{

    private Button btnRegisterPlace;
    private ImageView imgBtnSelImage;
    private TextView txtPlaceName;
    private EditText editDescription;
    private Bitmap bmpImage;
    private String UPLOAD_URL = "http://inhaleo.co/api/Place/api.php";
    private String imagePath;

    private String strImageFolder = "Images";
    private String strCaption = null;
    private String strPath="";
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_server);

        initUI();
    }

    private void initUI(){

        File imagesFolder = new File(Environment.getExternalStorageDirectory(), strImageFolder);
        if (imagesFolder.exists()) {

        } else {
            imagesFolder.mkdirs();
        }
        imagePath = "";
        btnRegisterPlace = (Button)findViewById(R.id.regiserServerBtn);
        imgBtnSelImage = (ImageView) findViewById(R.id.registerServerImgPlus);
        txtPlaceName = (TextView)findViewById(R.id.txtPlaceName);
        editDescription = (EditText)findViewById(R.id.registerServerEditComment);
        txtPlaceName.setText(tempItem.placeName);

        btnRegisterPlace.setOnClickListener(this);
        imgBtnSelImage.setOnClickListener(this);

        bmpImage = null;
        editDescription.setText("");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.regiserServerBtn):{
                String description = editDescription.getText().toString();
                if(imagePath.equals("")){
                    Toast.makeText(RegisterServerActivity.this, "Please select image and write the description!", Toast.LENGTH_LONG).show();
                }
                else{
                    tempItem.placeDescription = description;
                    registerPlaceToServer();
                }
                break;
            }
            case (R.id.registerServerImgPlus):{
                selectImage();
                break;
            }
        }
    }

    private void registerPlaceToServer(){
        final ProgressDialog loading = ProgressDialog.show(this,"Registering...","Please wait...", false, false);
    //Showing the progress dialog
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        int placeID = Integer.valueOf(s);
                        if(placeID < 1){
                            Toast.makeText(RegisterServerActivity.this, "Failed! Please try again!", Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(RegisterServerActivity.this, "Register Success!", Toast.LENGTH_LONG).show();
                            RegisterServerActivity.this.finish();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        //Showing toast
                        Toast.makeText(RegisterServerActivity.this, "Failed! Please try again!", Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();

                DateFormat df = new SimpleDateFormat("dd MM yyyy, HH:mm");
                tempItem.placeDate = df.format(Calendar.getInstance().getTime());
//                String placeImage = getStringImage(bmpImage);
                Bitmap bitmap=getBitmapByPath(imagePath, 400, 600);
                //Converting Bitmap to String
                String image_decode = getStringImage(bitmap);

               //Adding parameters
                params.put("request", "1");
                params.put("placeName", tempItem.placeName);
                params.put("placeAddress", tempItem.placeAddress);
                params.put("placeDescription", tempItem.placeDescription);
                params.put("placeDate", tempItem.placeDate);
                params.put("placeRating", "0");
                params.put("placeLatitude", tempItem.placeLatitude);
                params.put("placeLongitude", tempItem.placeLongitude);
                params.put("placeWebsite", tempItem.placeWebsite);
                params.put("placePhone", tempItem.placePhone);
                params.put("placeImage", image_decode);
                params.put("pImgName", "image");
                params.put("placeRatingCount", "0");

                //returning parameters
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
       // stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 3, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void selectImage(){

        final CharSequence[] items = { "Take Photo from Camera", "Choose from Library", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterServerActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo from Camera")) {

//                    ImageUtils.openCameraImage(RegisterServerActivity.this);
                    SimpleDateFormat strStamp = new SimpleDateFormat("yyyy-MM-DD_HHmmss");
                    strCaption = strStamp.format(new Date());

                    File file = new File(Environment.getExternalStorageDirectory()+ File.separator + strImageFolder + File.separator + "image.png");
                    Uri outputFileUri = Uri.fromFile(file);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Choose from Library")) {

//                    ImageUtils.openLocalImage(RegisterServerActivity.this);
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (resultCode == RESULT_CANCELED) {
//            return;
//        }
//
//        switch (requestCode) {
//
//            case ImageUtils.GET_IMAGE_BY_CAMERA:
//                if(ImageUtils.imageUriFromCamera != null) {
//                    ImageUtils.cropImage(this, ImageUtils.imageUriFromCamera);
//                    break;
//                }
//                break;
//            case ImageUtils.GET_IMAGE_FROM_PHONE:
//                if(data != null && data.getData() != null) {
//                    ImageUtils.cropImage(this, data.getData());
//                }
//                break;
//            case ImageUtils.GET_IMAGE_FROM_FACEBOOK:
//                if(data != null && data.getData() != null) {
//                    ImageUtils.cropImage(this, data.getData());
//                }
//                break;
//
//            case ImageUtils.CROP_IMAGE:
//                if(ImageUtils.cropImageUri != null) {
//                    bmpImage = null;
//                    imgBtnSelImage.setImageURI(ImageUtils.cropImageUri);
//                    bmpImage =((BitmapDrawable)imgBtnSelImage.getDrawable()).getBitmap();
//
////                    String aaa = BitmapUtil.compressImageUpload(ImageUtils.cropImageUri.toString());
////                    Toast.makeText(this, aaa, Toast.LENGTH_LONG).show();
//                }
//                break;
//            default:
//                break;
//        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
                imgBtnSelImage.setBackgroundDrawable(getResources().getDrawable(R.color.white));
            }
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            imgBtnSelImage.setBackgroundDrawable(getResources().getDrawable(R.color.white));
        }
    }
    private void onCaptureImageResult(Intent data) {
        Bitmap mBitmap = getBitmapByPath(Environment.getExternalStorageDirectory()+ File.separator + strImageFolder + File.separator + "image.png",400 ,600 );
        onSavePhoto(mBitmap);
        if (new File(Environment.getExternalStorageDirectory()+File.separator + strImageFolder + File.separator + strCaption + ".png").exists()) {
            imgBtnSelImage.setImageBitmap(BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + File.separator + strImageFolder + File.separator + strCaption + ".png"));
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        imagePath=selectedImagePath;
        imgBtnSelImage.setImageBitmap(bm);
    }

    public void onSavePhoto(Bitmap bmpData) {
        strPath = Environment.getExternalStorageDirectory()+File.separator + strImageFolder + File.separator + strCaption + ".png";

        imagePath=strPath;

        File fImageFile = new File(Environment.getExternalStorageDirectory()+File.separator + strImageFolder + File.separator + strCaption + ".png");
        if (fImageFile.exists())
            fImageFile.delete();

        try {
            if (!fImageFile.createNewFile()) {
                fImageFile = null;
                bmpData.recycle();
                return;
            } else {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bmpData.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                FileOutputStream fo;
                try {
                    fImageFile.createNewFile();
                    fo = new FileOutputStream(fImageFile);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            fImageFile = null;
            bmpData.recycle();
            return;
        }
    }
    public static Bitmap getBitmapByPath(String file, float screenWidth, float screenHeight) {
        File f = new File(file);

        Bitmap b = null;

        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            FileInputStream fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();

            int scale = 1;
            if (o.outHeight > screenHeight || o.outWidth > screenWidth) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(screenHeight / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();


            Bitmap thmbnail = null;

            int width = b.getWidth();
            int height = b.getHeight();

            float scaleWidth = 0;
            float scaleHeight = 0;

            Matrix matrix = new Matrix();

            if (width > height) {
                scaleWidth = (float)(screenWidth / height);
                scaleHeight = (float)((width * screenWidth / height) / width);

//				matrix.postRotate(90);
            }
            else {
                scaleWidth = (float)(screenWidth / width);
                scaleHeight = (float)((height * screenWidth / width) / height);
            }

            matrix.postScale(scaleWidth, scaleHeight);

            thmbnail = Bitmap.createBitmap(b, 0, 0, width, height, matrix, true);

            return thmbnail;

        } catch (IOException e) {

        }

        return null;
    }
}
