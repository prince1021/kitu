package com.kitu.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.reflect.TypeToken;
import com.kitu.Item.PlaceItem;
import com.kitu.Item.tempItem;
import com.kitu.Module.JSONUtils;
import com.kitu.R;

import com.kitu.Adapter.PlaceAutocompleteAdapter;
import com.kitu.Logger.Log;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RegisterPlaceActivity extends SampleActivityBase implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener {

    private GoogleMap mMap;
    LatLng myPosition;
    AlertDialog mAlertDialog;
    private Location mCurrentLocation;
    private final int[] MAP_TYPES = {GoogleMap.MAP_TYPE_SATELLITE,
            GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_HYBRID,
            GoogleMap.MAP_TYPE_TERRAIN,
            GoogleMap.MAP_TYPE_NONE};

    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    protected GoogleApiClient mGoogleApiClient;
    protected GoogleApiClient mGoogleApiClient1;
    private PlaceAutocompleteAdapter mAdapter;
    private AutoCompleteTextView mAutocompleteView;
    private Button btnNext;
    private String UPLOAD_URL = "http://inhaleo.co/api/Place/api.php";
    public ArrayList<PlaceItem> listPlace=new ArrayList<PlaceItem>();
    Map <String, Integer> mMarkers = new HashMap<String, Integer>();

    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.kitu.R.layout.activity_register_place);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btnNext = (Button) findViewById(R.id.registerMapBtnNext);
        btnNext.setOnClickListener(this);

        // Construct a GoogleApiClient for the {@link Places#GEO_DATA_API} using AutoManage
        // functionality, which automatically sets up the API client to handle Activity lifecycle
        // events. If your activity does not extend FragmentActivity, make sure to call connect()
        // and disconnect() explicitly.
        mGoogleApiClient1 = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        // Retrieve the AutoCompleteTextView that will display Place suggestions.
        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.autoPlaceFindText);

        // Register a listener that receives callbacks when a suggestion has been selected
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        // Retrieve the TextViews that will display details and attributions of the selected place.

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient1, BOUNDS_GREATER_SYDNEY, null);
        mAutocompleteView.setAdapter(mAdapter);
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient1, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            // Format details of the place for display and show it in a TextView.
            String strPlace =String.valueOf(formatPlaceDetails(getResources(), place.getName(),
                    place.getId(), place.getAddress(), place.getPhoneNumber(),
                    place.getWebsiteUri()));

            String searchAddress = place.getAddress().toString();
            String searchPlaceName = place.getName().toString();
            String searchPhone = place.getPhoneNumber().toString();
            String searchWebsite;
            if (place.getWebsiteUri() == null){
                searchWebsite = "";
            }
            else{
                searchWebsite = place.getWebsiteUri().toString();
            }
            LatLng searchPlaceCoordinate = place.getLatLng();
            Double searchLati = searchPlaceCoordinate.latitude;
            Double searchLong = searchPlaceCoordinate.longitude;

            display_maker(searchAddress, searchLati,searchLong);

            tempItem.placeAddress = searchAddress;
            tempItem.placeName = searchPlaceName;
            tempItem.placeWebsite = searchWebsite;
            tempItem.placePhone = searchPhone;
            tempItem.placeLatitude = searchLati.toString();
            tempItem.placeLongitude = searchLong.toString();

            Log.i(TAG, "Place details received: " + place.getName());

            places.release();
        }
    };

    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        Log.e(TAG, res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setPadding(0, 0, 0, 0);
        mMap.setOnMapClickListener(this);
        mMap.getUiSettings().setZoomControlsEnabled(false);

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            public void onInfoWindowClick(Marker marker) {
                int id = mMarkers.get(marker.getId());
                if(id != -1){
                    // Do stuff with the id
                    PlaceItem place_Item = new PlaceItem();
                    tempItem temp_Item = new tempItem();
                    place_Item = listPlace.get(id);
                    temp_Item.placeID = place_Item.getPlaceID();
                    temp_Item.placeName = place_Item.getPlaceName();
                    temp_Item.placeAddress = place_Item.getPlaceAddress();
                    temp_Item.placeWebsite = place_Item.getPlaceWebsite();
                    temp_Item.placePhone = place_Item.getPlacePhone();
                    temp_Item.placeLatitude = place_Item.getPlaceLatitude();
                    temp_Item.placeLongitude = place_Item.getPlaceLongitude();

                    Intent intentDetailList = new Intent(RegisterPlaceActivity.this, DetailListsActivity.class);
                    startActivity(intentDetailList);
                    RegisterPlaceActivity.this.overridePendingTransition(R.anim.slide_in_from_right,
                            R.anim.slide_out_to_left);
                }
            }
        });

//        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
//        Double sel_latitude = latLng.latitude;
//        Double sel_longitude = latLng.longitude;
//
//        String address = get_Address(sel_latitude, sel_longitude);
//        Toast.makeText(this, address, Toast.LENGTH_LONG).show();
    }

    private String get_Address(Double latitude, Double longitude) {
        /////////---------Second Way-------------/////////////////
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(", ");
                }
                strAdd = strReturnedAddress.toString();
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }


    public void display_maker(String daddress, Double dlatitude, Double dlongitude) {
//        mMap.clear();
        LatLng Riyadh = new LatLng(dlatitude, dlongitude);
        Marker mkr = mMap.addMarker(new MarkerOptions()
                .position(Riyadh)
                .title(daddress)
        );
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Riyadh));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Riyadh, 15));
        mMarkers.put(mkr.getId(), -1);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        tempItem.placeLatitude = "";
        tempItem.placeLongitude = "";
        tempItem.placeName = "";
        tempItem.placeAddress = "";
        tempItem.placeWebsite = "";
        tempItem.placeDescription = "";
        tempItem.placeImage = "";
        mAutocompleteView.setText("");
        getAllPlaces();
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        // check if GPS enabled
        if (location != null) {

            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            if (latitude == 0.0 && longitude == 0.0) {
                mAlertDialog = new AlertDialog.Builder(RegisterPlaceActivity.this)
                        .setTitle("Warnning")
                        .setMessage("Google play service is not available! Please try again!")
//                        .setIcon(getResources().getDrawable(R.drawable.pin))
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
                LatLng Riyadh = new LatLng(40.712784, -74.005941);
                mMap.addMarker(new MarkerOptions()
                        .position(Riyadh)
                        .title("Marker in Riyadh")
                );

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Riyadh));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Riyadh, 15));
            } else {
                LatLng latLng = new LatLng(latitude, longitude);

                myPosition = new LatLng(latitude, longitude);
                Marker mkr = mMap.addMarker(new MarkerOptions().position(myPosition)
                        .title("Current User location")
                );
                mMap.moveCamera(CameraUpdateFactory.newLatLng(myPosition));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 15));

                mMarkers.put(mkr.getId(), -1);


                // \n is for new line
//                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            }

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
//            gps.showSettingsAlert();
        }

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.registerMapBtnNext): {
                String strAutoText = mAutocompleteView.getText().toString();
                String aaa = tempItem.placeAddress;
                String bbb = tempItem.placeLatitude;
                String ccc = tempItem.placeLongitude;
                String ddd = tempItem.placeName;

                if(tempItem.placeAddress.equals("") || tempItem.placeName.equals("") || tempItem.placeLatitude.equals("") || tempItem.placeLongitude.equals("") || strAutoText.equals("") ){
                    Toast.makeText(RegisterPlaceActivity.this, "The place you selected is not correct! Please try again!", Toast.LENGTH_LONG).show();
                }
                else{
                    Intent intentNext = new Intent(RegisterPlaceActivity.this, RegisterServerActivity.class);
                    startActivity(intentNext);
                    RegisterPlaceActivity.this.overridePendingTransition(R.anim.slide_in_from_right,
                            R.anim.slide_out_to_left);
                }
                break;
            }
        }
    }

    private void getAllPlaces(){
        final ProgressDialog loading = ProgressDialog.show(this,"Loading...","Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
//                        Toast.makeText(SearchFunnyPlaceActivity.this, s, Toast.LENGTH_LONG).show();

                        Type type = new TypeToken<List<PlaceItem>>() {
                        }.getType();
                        listPlace = JSONUtils.parseJSONArray(s, type);
                        showMarker();
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(RegisterPlaceActivity.this, "Failed! Please try again!", Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("request", "2");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    private void showMarker(){
        for (int i=0; i<listPlace.size(); i++){
            PlaceItem pItem = listPlace.get(i);
            LatLng Riyadh = new LatLng(Double.parseDouble(pItem.getPlaceLatitude()), Double.parseDouble(pItem.getPlaceLongitude()));
            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.pin1);

            Marker mkr = mMap.addMarker(new MarkerOptions()
                    .position(Riyadh)
                    .title(pItem.getPlaceName())
                    .snippet(pItem.getPlaceAddress())
                    .icon(icon)
            );
            mMarkers.put(mkr.getId(), i);
        }
    }



}