package com.kitu.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.places.Place;
import com.google.gson.reflect.TypeToken;
import com.kitu.Adapter.DetailListsAdapter;
import com.kitu.Item.PlaceItem;
import com.kitu.Item.tempItem;
import com.kitu.Module.JSONUtils;
import com.kitu.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by PRINCE on 11/30/2016.
 */

public class DetailListsActivity extends Activity{

    private ListView listViewPlace;
    private DetailListsAdapter adapterListPlace;
    private TextView txtPlaceName;
    private TextView txtPlaceAddr;
    private TextView txtPlaceLati;
    private TextView txtPlaceLongi;

    private String UPLOAD_URL = "http://inhaleo.co/api/Place/api.php";
    public ArrayList<PlaceItem> detailPlaceLists=new ArrayList<PlaceItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_lists);
    }

    @Override
    protected void onResume() {
        super.onResume();

        initUI();
        getDetailPlaceLists();

        listViewPlace.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                PlaceItem pItem = detailPlaceLists.get(position);
                tempItem.placeID = pItem.getPlaceID();
                tempItem.placeRating = pItem.getPlaceRating();
                tempItem.placeRatingCount = pItem.getPlaceRatingCount();
                tempItem.placeName = pItem.getPlaceName();
                tempItem.placeDescription = pItem.getPlaceDescription();
                tempItem.placeImage = pItem.getPlaceImage();
                Intent intentDetail = new Intent(DetailListsActivity.this, DetailPlaceActivity.class);
                startActivity(intentDetail);
                DetailListsActivity.this.overridePendingTransition(R.anim.slide_in_from_right,
                        R.anim.slide_out_to_left);
            }
        });
    }

    private void initUI(){

        listViewPlace = (ListView)findViewById(R.id.detailList);
        txtPlaceAddr = (TextView)findViewById(R.id.detailListTxtAddress);
        txtPlaceName = (TextView)findViewById(R.id.detailListTxtName);
        txtPlaceLati = (TextView)findViewById(R.id.detailListTxtLati);
        txtPlaceLongi = (TextView)findViewById(R.id.detailListTxtLong);

        txtPlaceAddr.setText(tempItem.placeAddress);
        txtPlaceName.setText(tempItem.placeName);
        txtPlaceLati.setText(tempItem.placeLatitude);
        txtPlaceLongi.setText(tempItem.placeLongitude);
    }

    private void getDetailPlaceLists(){
        final ProgressDialog loading = ProgressDialog.show(this,"Loading...","Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        Type type = new TypeToken<List<PlaceItem>>() {
                        }.getType();
                        detailPlaceLists = JSONUtils.parseJSONArray(s, type);

                        for (int i=0; i<detailPlaceLists.size(); i++){

                            adapterListPlace = new DetailListsAdapter(DetailListsActivity.this, R.layout.activity_detail_lists_adapter, detailPlaceLists);
                            listViewPlace.setAdapter(adapterListPlace);

                            adapterListPlace.notifyDataSetChanged();
                        }
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(DetailListsActivity.this, "Loading Places failed! Please try again!", Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("request", "4");
                params.put("placeLatitude", tempItem.placeLatitude);
                params.put("placeLongitude", tempItem.placeLongitude);
                return params;
            }
        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
}
