package com.kitu.Activities;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kitu.*;
import com.kitu.R;

import static com.kitu.Activities.RegisterPlaceActivity.MY_PERMISSIONS_REQUEST_LOCATION;

/**
 * Created by PRINCE on 11/15/2016.
 */

public class LogoActivity extends FragmentActivity implements View.OnClickListener {
    private ViewPager mPager;

    private ImageView mImageView;
    private ImageView mImgCover, mGrayView;
    private RelativeLayout loginLinear;
    private boolean isDragging;
    private boolean isFoucusRight;
    private int mLastPos;
    private Button btnSearch;
    private Button btnRegister;
    private Button btnLogin;
    private EditText editLogin;
    private ImageButton btnCancel;
    private TextView txtHelp;

//    Context context;

    private int[] logBanner = new int[]{com.kitu.R.drawable.kitu1,
            R.drawable.kitu2, R.drawable.kitu3,
            R.drawable.kitu4};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        mImgCover = (ImageView) findViewById(R.id.img_cover);
        mImageView = (ImageView) findViewById(R.id.img_indicator01);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkStoragePermission();
        }

        initUI();

        initPager();
        autoScroll();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.logo_btnSearch): {
                Intent intentSearchPlace = new Intent(LogoActivity.this, SearchFunnyPlaceActivity.class);
                startActivity(intentSearchPlace);
                LogoActivity.this.overridePendingTransition(R.anim.slide_in_from_right,
                        R.anim.slide_out_to_left);
                break;
            }
            case (R.id.logo_btnRegister): {
                // show login dialog
                setAction(false);
                break;
            }
            case (R.id.logo_help): {
                Intent intentHelper = new Intent(LogoActivity.this, HelpActivity.class);
                startActivity(intentHelper);
                LogoActivity.this.overridePendingTransition(R.anim.slide_in_from_right,
                        R.anim.slide_out_to_left);
                break;
            }
            case (R.id.btnCancel): {
                //hide login dialog
                setAction(true);
                break;
            }
            case (R.id.btnLogin):{
                if(editLogin.getText().toString().equals("2505")){
                    setAction(true);
                    editLogin.setText("");
                    Intent intentHelper = new Intent(LogoActivity.this, RegisterPlaceActivity.class);
                    startActivity(intentHelper);
                    LogoActivity.this.overridePendingTransition(R.anim.slide_in_from_right,
                            R.anim.slide_out_to_left);
                }
                else{
                    editLogin.setText("");
//                    Toast.makeText(LogoActivity.this, "Incorrect Password! Please try again!", Toast.LENGTH_LONG).show();
                    Toast.makeText(LogoActivity.this, "Incorrect Password! Please try again!", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    private void initUI() {
        btnSearch = (Button) findViewById(R.id.logo_btnSearch);
        btnRegister = (Button) findViewById(R.id.logo_btnRegister);
        txtHelp = (TextView) findViewById(R.id.logo_help);

        txtHelp.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        mGrayView = (ImageView)findViewById(R.id.imageGray);
        loginLinear = (RelativeLayout)findViewById(R.id.loginLinear);

        btnCancel = (ImageButton)findViewById(R.id.btnCancel);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        editLogin = (EditText)findViewById(R.id.txtLogin);
        btnLogin.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

    }

    private void initPager() {

        mPager = (ViewPager) findViewById(R.id.pager_banner);
        FragmentManager fm = getSupportFragmentManager();
        MyPagerAdapter adapter = new MyPagerAdapter(fm);
        mPager.setAdapter(adapter);
        mPager.setCurrentItem(1500);
        mPager.setOnPageChangeListener(new MyPagerListener());
    }

    private void autoScroll() {
        mPager.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (!isDragging) {
                    mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                }

                isFoucusRight = !isFoucusRight;
                mPager.postDelayed(this, 3000);
            }
        }, 3000);

    }
    private void setAction(boolean flag)
    {
        if(!flag)
        {
            mGrayView.setVisibility(View.VISIBLE);
            loginLinear.setVisibility(View.VISIBLE);
        }else
        {
            mGrayView.setVisibility(View.GONE);
            loginLinear.setVisibility(View.GONE);
        }
        btnRegister.setEnabled(flag);
        btnSearch.setEnabled(flag);
        txtHelp.setEnabled(flag);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        android.os.Process.killProcess(android.os.Process.myPid());
        finish();
        System.exit(0);

    }

    class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            BannerItemFragment fragment = new BannerItemFragment();
            fragment.setResId(logBanner[position % logBanner.length]);
            return fragment;
        }

        @Override
        public int getCount() {
            return 10000;
        }

    }

    class MyPagerListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset,
                                   int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            int width = mImgCover.getWidth();
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mImageView
                    .getLayoutParams();
            int rightMargin = layoutParams.rightMargin;
            int endPos = (width + rightMargin) * (position % 4);
            int startPos = 0;
            if (mLastPos < position) {
                startPos = (width + rightMargin) * (position % 4 - 1);
            } else {
                startPos = (width + rightMargin) * (position % 4 + 1);
            }
            ObjectAnimator.ofFloat(mImgCover, "translationX", startPos, endPos)
                    .setDuration(300).start();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    public boolean checkStoragePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
}