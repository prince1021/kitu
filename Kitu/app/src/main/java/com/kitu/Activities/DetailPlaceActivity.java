package com.kitu.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.reflect.TypeToken;
import com.kitu.Adapter.DetailListsAdapter;
import com.kitu.Item.PlaceItem;
import com.kitu.Item.tempItem;
import com.kitu.Module.JSONUtils;
import com.kitu.R;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by PRINCE on 11/30/2016.
 */

public class DetailPlaceActivity extends Activity {

    private Button btnSetRating;
    private TextView txtPlaceName;
    private TextView txtPlaceDescription;
    private ImageView imgPlace;
    private RatingBar ratingMark;
    Integer DIALOG_ID =1;
    String settedMark;
    Float float_mark;
    Float float_current_mark;
    int int_review_count;
    private String UPLOAD_URL = "http://inhaleo.co/api/Place/api.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_place);

        settedMark = "0";
        btnSetRating = (Button)findViewById(R.id.detailPlaceSetRatingButton);
        txtPlaceName = (TextView)findViewById(R.id.detailPlaceTxtName);
        txtPlaceDescription = (TextView)findViewById(R.id.detailPlaceTxtDescription);
        imgPlace = (ImageView)findViewById(R.id.detailPlaceImage);
        ratingMark = (RatingBar)findViewById(R.id.detailPlaceRating);

        txtPlaceName.setText(tempItem.placeName);
        txtPlaceDescription.setText(tempItem.placeDescription);

        float_mark = Float.parseFloat(tempItem.placeRating);
        int_review_count = Integer.parseInt(tempItem.placeRatingCount);

        Picasso.with(DetailPlaceActivity.this)
                .load(tempItem.placeImage)
                .placeholder(R.drawable.bg_cover_userheader) // optional
                .error(R.drawable.bg_cover_userheader)         // optional
                .into(imgPlace);

        ratingMark.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            // Called when the user swipes the RatingBar
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//                tv.setText("Rating:" + rating);
                float_current_mark = rating;
                settedMark = String.valueOf(rating);
            }
        });

        btnSetRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(settedMark.equals("0")){
                    Toast.makeText(DetailPlaceActivity.this, "Please set the rating of this image!", Toast.LENGTH_LONG).show();
                }
                else{
                    setRating();
                }
            }
        });
    }

    private void setRating(){
        if(float_mark == 0.0){
            float_mark = (float)1.0;
        }
        final int updatedCount = int_review_count + 1;
        final Float updatedMark = (float_mark * int_review_count+float_current_mark)/updatedCount;
        final ProgressDialog loading = ProgressDialog.show(this,"Loading...","Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
//                        Toast.makeText(DetailPlaceActivity.this, s , Toast.LENGTH_LONG).show();
                        DetailPlaceActivity.this.finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(DetailPlaceActivity.this, "Failed! Please try again!", Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("request", "3");
                params.put("placeRating", String.valueOf(updatedMark));
                params.put("placeRatingCount", String.valueOf(updatedCount));
                params.put("placeID", tempItem.placeID);
                return params;
            }
        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
}
