package com.kitu.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.kitu.Item.PlaceItem;
import com.kitu.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by PRINCE on 11/30/2016.
 */

public class DetailListsAdapter  extends ArrayAdapter<PlaceItem> {
    private Activity context;
    private int mResource;
    private ArrayList<PlaceItem> itemName;
    AlertDialog mAlertDialog;

    public DetailListsAdapter(Activity context, int itemResourceId,
                          ArrayList<PlaceItem> list) {
        super(context, itemResourceId, list);
        this.context = context;
        this.mResource = itemResourceId;
        this.itemName = list;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = null;
        if (null == convertView) {
            LayoutInflater inflater = context.getLayoutInflater();
            v = inflater.inflate(mResource, null);

        } else {
            v = convertView;
        }
        PlaceItem placeItem = itemName.get(position);

        TextView datetxt = (TextView) v.findViewById(R.id.adapterTxtDate);
        TextView reviewtxt = (TextView) v.findViewById(R.id.adapterTxtCount);
        ImageView placeImage=(ImageView)v.findViewById(R.id.imgPlace);
        TextView placeDescription = (TextView)v.findViewById(R.id.adapterTxtDescription);
        RatingBar ratePlace = (RatingBar)v.findViewById(R.id.adapterRating);

        Float placeRating = Float.parseFloat(placeItem.getPlaceRating());
        datetxt.setText(placeItem.getPlaceDate());
        reviewtxt.setText(placeItem.getPlaceRatingCount()+" reviewed");
        placeDescription.setText(placeItem.getPlaceDescription());
//        placeImage.setBackground(this.getContext().getDrawable(R.drawable.demo));
        ratePlace.setRating(placeRating);

        Picasso.with(getContext())
                .load(placeItem.getPlaceImage())
                .placeholder(R.drawable.bg_cover_userheader) // optional
                .error(R.drawable.bg_cover_userheader)         // optional
                .into(placeImage);

        return v;
    }
}