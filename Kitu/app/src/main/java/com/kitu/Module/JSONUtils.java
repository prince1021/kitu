package com.kitu.Module;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public final class JSONUtils {
    public static <T> T parseJSON(String jsonStr, Class<T> t) {
        Gson gson = new Gson();
        T bean = gson.fromJson(jsonStr, t);
        return bean;
    }


    public static <T> T parseJSONArray(String response, Type type) {
        Gson gson = new Gson();
        T data = gson.fromJson(response, type);
        return data;
    }

    private JSONUtils() {
    }
}