package com.kitu.Item;

/**
 * Created by PRINCE on 11/30/2016.
 */

public class PlaceItem {

    private String id;
    private String placeName;
    private String placeAddress;
    private String placeDescription;
    private String placeDate;
    private String placeRating;
    private String placeLatitude;
    private String placeLongitude;
    private String placeImage;
    private String placeWebsite;
    private String placePhone;
    private String placeFullName;
    private String placeRatingCount;

    public String getPlaceID(){ return  id; }
    public String getPlaceDescription(){ return  placeDescription;};
    public String getPlaceDate(){ return placeDate;};
    public String getPlaceRating(){ return placeRating;};
    public String getPlaceLatitude(){ return placeLatitude;};
    public String getPlaceLongitude(){return placeLongitude;};
    public String getPlaceImage(){ return placeImage;};
    public String getPlaceName(){ return placeName;};
    public String getPlaceAddress(){ return placeAddress;};
    public String getPlaceWebsite(){ return placeWebsite;};
    public String getPlacePhone(){ return placePhone;};
    public String getPlaceRatingCount(){ return placeRatingCount;};

    public void setPlaceID(String value){ this.id = value;};
    public void setPlaceDescription(String value){this.placeDescription = value;};
    public void setPlaceDate(String value){this.placeDate = value;};
    public void setPlaceRating(String value){this.placeRating = value;};
    public void setPlaceLatitude(String value){this.placeLatitude = value;};
    public void setPlaceLongitude(String value){this.placeLongitude = value;};
    public void setPlaceImage(String value){this.placeImage = value;};
    public void setPlaceName(String value){this.placeName = value;};
    public void setPlaceAddress(String value){this.placeAddress = value;};
    public void setPlaceWebsite(String value){this.placeWebsite = value;};
    public void setPlacePhone(String value){this.placePhone = value;};
    public void setPlaceRatingCount(String value){this.placeRatingCount = value;};

}
