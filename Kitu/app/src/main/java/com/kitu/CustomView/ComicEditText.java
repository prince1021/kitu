package com.kitu.CustomView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by PRINCE on 12/2/2016.
 */

public class ComicEditText extends EditText {
    public ComicEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "comic.ttf");
        this.setTypeface(tf);
    }
}
