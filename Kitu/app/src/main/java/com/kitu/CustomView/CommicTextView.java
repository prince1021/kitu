package com.kitu.CustomView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import org.w3c.dom.Attr;

/**
 * Created by PRINCE on 12/2/2016.
 */

public class CommicTextView extends TextView {
    public CommicTextView(Context context, AttributeSet attr) {
        super(context, attr);
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "comic.ttf");
        this.setTypeface(tf);
    }
}
